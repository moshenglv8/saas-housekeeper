/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller.request;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 消息已读入参
 *
 * @since 2022-02-22
 */
@Getter
@Setter
@ApiModel("消息已读")
public class ReadMsgDto {
    @ApiModelProperty(value = "已读的消息Id", required = true)
    private List<String> userIdList;
}
