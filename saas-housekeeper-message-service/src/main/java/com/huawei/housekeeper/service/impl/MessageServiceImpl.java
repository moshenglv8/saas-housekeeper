/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.service.impl;

import com.huawei.housekeeper.constant.Constant;
import com.huawei.housekeeper.utils.TokenUtil;
import com.huawei.housekeeper.dao.entity.Message;
import com.huawei.housekeeper.dao.mapper.MessageMapper;
import com.huawei.housekeeper.service.MessageService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 接口实现类
 *
 * @since 2022-02-15
 */
@Service
public class MessageServiceImpl implements MessageService {

    @Autowired
    private MessageMapper messageMapper;

    @Autowired
    private TokenUtil tokenUtil;

    /**
     * 获取所有消息
     *
     * @return 消息列表
     */
    @Override
    public List<Message> getAllMsg() {
        QueryWrapper<Message> queryWrapper = new QueryWrapper<>();
        String token = tokenUtil.getUidFromHeader();
        queryWrapper.eq("user_id", token);
        queryWrapper.ne("message_status", Constant.MessageStatus.DEL);
        List<Message> messageList = messageMapper.selectList(queryWrapper);
        return messageList;
    }

    /**
     * 获取指定状态消息
     *
     * @param status 状态
     * @return 消息列表
     */
    @Override
    public List<Message> getMsg(Integer status) {
        QueryWrapper<Message> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", tokenUtil.getUidFromHeader());
        queryWrapper.eq("message_status", status);
        return messageMapper.selectList(queryWrapper);
    }

    /**
     * 获取未读消息数量
     *
     * @return 未读消息数量
     */
    @Override
    public int getMsgCount() {
        return messageMapper.getMsgCount(tokenUtil.getUidFromHeader());
    }

    /**
     * 修改消息状态
     *
     * @param idList 消息id
     * @param status 消息状态
     * @return 是否修改成功
     */
    @Override
    public boolean setMsgStatus(List<String> idList, Integer status) {
        return messageMapper.setMsgStatus(idList, status);
    }
}
