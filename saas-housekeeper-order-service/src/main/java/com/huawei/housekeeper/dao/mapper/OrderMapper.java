
/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.dao.mapper;

import com.huawei.housekeeper.dao.entities.Order;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import org.apache.ibatis.annotations.Mapper;

/**
 * 订单mapper
 *
 * @author lWX1128557
 * @since 2022-02-24
 */
@Mapper
public interface OrderMapper extends BaseMapper<Order> {
}
