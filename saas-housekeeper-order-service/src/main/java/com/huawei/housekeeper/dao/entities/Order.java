
/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.dao.entities;

import com.huawei.housekeeper.entity.BaseEntity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 订单表
 *
 * @author lWX1128557
 * @since 2022-03-01
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "t_order")
public class Order extends BaseEntity {

    /**
     * 订单号
     */
    @TableField(value = "ORDER_NUMBER")
    private String orderNumber;

    /**
     * 顾客ID
     */
    @TableField(value = "CUSTOMER_ID")
    private String customerId;

    /**
     * 顾客姓名
     */
    @TableField(value = "CUSTOMER_NAME")
    private String customerName;

    /**
     * 可选服务表ID
     */
    @TableField(value = "SKU_ID")
    private Integer skuId;

    /**
     * 服务价格
     */
    @TableField(value = "PRICE")
    private BigDecimal price;

    /**
     * 预约时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "APPOINTMENT_TIME")
    private Date appointmentTime;

    /**
     * 接单时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "ORDER_TIME")
    private Date orderTime;

    /**
     * 完成时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "COMPLETION_TIME")
    private Date completedTime;

    /**
     * 用户电话
     */
    @TableField(value = "CUSTOMER_PHONE")
    private String customerPhone;

    /**
     * 备注
     */
    @TableField(value = "REMARK")
    private String remark;

    /**
     * 订单数量
     */
    @TableField(value = "AMOUNT")
    private Integer amount;

    /**
     * 服务描述
     */
    @TableField(value = "SERVICE_DETAIL")
    private String serviceDetail;

    /**
     * 订单价格
     */
    @TableField(value = "PAYMENT")
    private BigDecimal payment;

    /**
     * 服务地址
     */
    @TableField(value = "ADDRESS")
    private String address;

    /**
     * 订单状态
     */
    @TableField(value = "STATUS")
    private Integer status;

    /**
     * 工人id
     */
    @TableField(value = "EMPLOYEE_ID")
    private String employeeId;

    /**
     * 工人名称
     */
    @TableField(value = "EMPLOYEE_NAME")
    private String employeeName;

    /**
     * 工人电话
     */
    @TableField(value = "EMPLOYEE_PHONE")
    private String employeePhone;
}
