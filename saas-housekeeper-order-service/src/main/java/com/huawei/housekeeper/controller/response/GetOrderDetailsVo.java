/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller.response;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 订单详情Vo
 *
 * @author lWX1128557
 * @since 2022-02-23
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "查询订单详情")
public class GetOrderDetailsVo {
    @ApiModelProperty(value = "订单ID")
    private Long orderId;

    @ApiModelProperty(value = "服务详情")
    private ServiceDetail serviceDetail;

    @ApiModelProperty(value = "订单编号")
    private String orderNumber;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty(value = "接单时间")
    private Date orderTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty(value = "完成时间")
    private Date completedTime;

    @ApiModelProperty(value = "订单数量")
    private Integer amount;

    @ApiModelProperty(value = "订单价格")
    private BigDecimal payment;

    @ApiModelProperty(value = "订单状态")
    private Integer status;

    @ApiModelProperty(value = "顾客姓名")
    private String customerName;

    @ApiModelProperty(value = "顾客电话")
    private String customerPhone;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty(value = "预约时间")
    private Date appointmentTime;

    @ApiModelProperty(value = "服务地址")
    private String address;

    @ApiModelProperty(value = "工人姓名")
    private String serverName;

    @ApiModelProperty(value = "工人电话")
    private String serverPhone;

    @ApiModelProperty(value = "服务图片")
    private String imgSrc;

    @ApiModelProperty(value = "备注")
    private String remark;

    /**
     * 服务详情
     *
     * @author lWX1128557
     * @since 2022-03-01
     */
    @Data
    public static class ServiceDetail {
        @ApiModelProperty(value = "服务名称")
        private String serviceName;

        @ApiModelProperty(value = "服务规格")
        private List<Specifications> specifications;
    }

    /**
     * 服务规格
     *
     * @author lWX1128557
     * @since 2022-03-01
     */
    @Data
    public static class Specifications {
        @ApiModelProperty(value = "规格名称")
        private String specName;

        @ApiModelProperty(value = "规格选项")
        private String optionName;
    }
}
