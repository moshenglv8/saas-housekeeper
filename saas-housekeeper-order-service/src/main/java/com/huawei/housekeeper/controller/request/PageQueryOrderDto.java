/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller.request;

import com.huawei.housekeeper.request.PageRequest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 * 分页Dto
 *
 * @author lWX1128557
 * @since 2022-02-23
 */
@ApiModel(value = "分页查询")
@Data
public class PageQueryOrderDto extends PageRequest {
    @Min(value = 1, message = "范围：1,2,3,4")
    @Max(value = 4, message = "范围：1,2,3,4")
    @ApiModelProperty(value = "订单状态", allowableValues = "1,2,3,4")
    private Integer status;

    @Length(max = 32, message = "范围：32")
    @ApiModelProperty(value = "客户姓名")
    private String customerName;
}
