
/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper;

import com.huawei.housekeeper.config.AutoConfiguration;
import com.huawei.saashousekeeper.properties.TenantProperties;

import lombok.extern.log4j.Log4j2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;

/**
 * 主启动类
 *
 * @author lWX1128557
 * @since 2022-03-03
 */
@ComponentScan(basePackages = "com.huawei.**", excludeFilters = {
    @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = {
        AutoConfiguration.class, TenantProperties.class
    })
})
@EnableEurekaClient
@EnableFeignClients
@Log4j2
@SpringBootApplication
public class SaasHousekeeperTenantinfoServiceApplication {

    /**
     * 租户启动入口
     *
     * @param args 形参
     */
    public static void main(String[] args) {
        log.info("tenant info module start...");
        SpringApplication.run(SaasHousekeeperTenantinfoServiceApplication.class, args);
        log.info("tenant info module start over...");
    }
}
