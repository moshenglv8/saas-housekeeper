package com.huawei.housekeeper.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huawei.housekeeper.controller.request.GetIdentifyCodeDto;
import com.huawei.housekeeper.dao.entities.TenantRegisterRecord;

/**
 * 功能描述
 *
 * @since 2022-10-20
 */
public interface RegisterRecordService extends IService<TenantRegisterRecord> {

    /**
     * 邮箱验证码
     *
     * @return 验证码
     */
    String getIdentifyCode(GetIdentifyCodeDto getIdentifyCodeDto);

}