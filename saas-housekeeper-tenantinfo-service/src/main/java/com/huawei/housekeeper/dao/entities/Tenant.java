
/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.dao.entities;

import com.huawei.housekeeper.entity.BaseEntity;

import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;

/**
 * 租户信息表
 *
 * @author lWX1128557
 * @since 2022-03-02
 */
@TableName(value = "tenant")
@Data
public class Tenant extends BaseEntity {

    /**
     * 租户id
     */
    private String tenantId;

    /**
     * 租户
     */
    private String name;

    /**
     * 租户企业代码
     */
    private String number;

    /**
     * 租户电话
     */
    private String phone;

    /**
     * 租户邮箱
     */
    private String email;

    /**
     * 内容
     */
    private String content;

    /**
     * 租户标识
     */
    private String tag;

    /**
     * 租户域名
     */
    private String domain;

    /**
     * 租户状态
     */
    private Integer status;
}
