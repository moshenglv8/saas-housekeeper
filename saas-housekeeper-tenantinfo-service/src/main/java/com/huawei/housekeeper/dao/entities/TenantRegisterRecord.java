package com.huawei.housekeeper.dao.entities;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 功能描述
 *
 * @since 2022-10-20
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName(value = "register_record", autoResultMap = true)
@ApiModel(value = "register_record", description = "register_record")
public class TenantRegisterRecord {
    /**
     * id
     */
    @TableField(value = "id")
    private Integer id;

    /**
     * delete_flag
     */
    @TableField(value = "delete_flag")
    private Long deleteFlag;

    /**
     * 邮箱
     */
    @TableField(value = "email")
    private String email;


}