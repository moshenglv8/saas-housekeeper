
package com.huawei.housekeeper.dao.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;

/**
 * 迁移信息
 *
 * @author lWX1128557
 * @since 2022-04-26
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MigrationInformation {
    /**
     * 迁移的总schema
     */
    private Integer total;

    /**
     * 迁移失败信息
     */
    private Map<String, String> failInfo;

    /**
     * 迁移成功数
     */
    private Integer successful;

    /**
     * 迁移失败数
     */
    private Integer failure;
}
