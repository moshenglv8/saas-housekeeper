/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 根据租户企业号更新租户状态
 *
 * @author lWX1128557
 * @since 2022-03-03
 */
@Getter
@Setter
@ApiModel(value = "更新租户状态")
public class UpdateTenantDto {
    @NotBlank
    @Length(max = 255, message = "长度范围：255")
    @ApiModelProperty(value = "租户企业信用代码", required = true)
    private String number;

    @NotNull
    @Min(value = 1, message = "范围：1,2,3")
    @Max(value = 3, message = "范围：1,2,3")
    @ApiModelProperty(value = "租户状态", required = true)
    private Integer status;
}
