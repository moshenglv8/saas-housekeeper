/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.constant;

/**
 * 租户状态常量
 *
 * @author lWX1128557
 * @since 2022-03-09
 */
public interface StatusConstant {
    /**
     * 申请中
     */
    int APPLING = 1;

    /**
     * 营业中
     */
    int ACTIVING = 2;

    /**
     * 已停业
     */
    int CLOSED = 3;
}
