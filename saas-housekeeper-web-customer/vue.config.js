// 租户域名
const TENANT_DOMAIN = 'http://sample1.saas-housekeeper.cloudbu.cloud-onlinelab.cn/';

/**
 * 用于修改请求头的 Referer 字段，后端是通过 Referer 的域名识别租户的
 * @param {*} proxyReq
 * @param {*} req
 * @param {*} res
 */
function onProxyReq(proxyReq, req, res) {
    proxyReq.setHeader('Referer', TENANT_DOMAIN);
}

module.exports = {
    publicPath: './',
    outputDir: 'dist',
    assetsDir: 'static',
    filenameHashing: true,
    lintOnSave: false,
    devServer: {
        hot: true,
        port: 8081,
        allowedHosts: 'all',
        proxy: {
            '/api-service': {
                target: TENANT_DOMAIN,
                onProxyReq,
            },
            '/api-admin': {
                target: TENANT_DOMAIN,
                onProxyReq,
            },
            '/api-log': {
                target: TENANT_DOMAIN,
            },
        },
    },
    // 自定义webpack配置
    configureWebpack: {},
};
