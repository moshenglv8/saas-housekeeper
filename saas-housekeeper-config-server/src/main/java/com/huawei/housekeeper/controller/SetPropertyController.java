/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */
package com.huawei.housekeeper.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.huawei.housekeeper.controller.request.SetPropertyDto;
import com.huawei.housekeeper.controller.result.Result;
import com.huawei.housekeeper.dao.mapper.PropertyMapper;

import io.swagger.annotations.ApiOperation;

/**
 * 增加配置
 *
 * @author y00464350
 * @since 2022-03-24
 */
@RestController
@RequestMapping("/property")
public class SetPropertyController {

    @Autowired
    private PropertyMapper propertyMapper;

    @PostMapping(value = "/add")
    @ApiOperation(value = "添加配置信息")
    public Result<Integer> addProperties(@RequestBody SetPropertyDto setPropertyDto) {
        return Result.createResult(propertyMapper.insertProperties(setPropertyDto));
    }
}
