import { getCookie } from '@/utils/utils';
import Axios from 'axios'

const fileRequest = Axios.create()
fileRequest.defaults.baseURL = '/api-service'

fileRequest.interceptors.request.use(config => {
    const token = getCookie('SaaS_Token');
    config.headers.Authorization = token;
    return config
})

export interface APIResponse<T = unknown> {
    code: number,
    message?: string,
    result?: T
}

/**
 * 上传图片
 * @param file 
 * @returns 
 */
export function uploadImage(file: File) {
    const formData = new FormData()
    formData.append('file', file)
    return fileRequest.post<APIResponse<string>>('/saas-public/housekeeper/image/upload', formData)
}

export function getImage(name: string) {
    return fileRequest.get<Blob>('/saas-public/housekeeper/image', {
        params: { name: name },
        responseType: 'blob'
    })
}