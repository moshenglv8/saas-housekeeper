/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 租户配置表
 *
 * @author jwx1116205
 * @since 2022-03-03
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("t_customization")
public class Customization {

    /**
     * 配置ID
     */
    @TableId(value = "config_id", type = IdType.NONE)
    private Long configId;

    /**
     * 主题标识
     */
    @TableField("style_flag")
    private Long styleFlag;

    /**
     * 店名
     */
    @TableField("store_name")
    private String storeName;

    /**
     * 租户标识
     */
    @TableField("tenant_id")
    private String tenantId;

}