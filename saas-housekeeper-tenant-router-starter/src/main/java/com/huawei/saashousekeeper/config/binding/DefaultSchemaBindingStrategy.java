/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.saashousekeeper.config.binding;

import com.huawei.saashousekeeper.properties.DataSourceBindingProperty;
import com.huawei.saashousekeeper.properties.DynamicSourceProperties;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * 默认schema转换逻辑，未自定义情况下使用此默认转换逻辑
 *
 * @author lWX1156935
 * @since 2022/4/22
 */
public class DefaultSchemaBindingStrategy implements SchemaBindingStrategy {
    @Autowired
    private DynamicSourceProperties dynamicSourceProperties;

    @Override
    public String getSchema(String key) {
        // 租户标识获取配置的对应schema以及数据源绑定信息
        DataSourceBindingProperty bindingProperty = dynamicSourceProperties.getBindingMap() != null
            ? dynamicSourceProperties.getBindingMap().getOrDefault(key, null)
            : null;
        String catalog = null;
        if (bindingProperty != null && StringUtils.isNotBlank(bindingProperty.getSchema())) {
            // 配置指定的schema, 优先级最高
            catalog = bindingProperty.getSchema();
        }
        return catalog;
    }
}
