/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.saashousekeeper.dbpool;

/**
 * jdbc连接池
 *
 * @author lWX1156935
 * @since 2022/4/22
 */
public interface PoolStrategy {
    /**
     * 获取池名称标识池类型
     *
     * @return 池名称
     */
    String getPoolName();
}
