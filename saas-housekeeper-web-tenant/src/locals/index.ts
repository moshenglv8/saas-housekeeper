import zhCN from './zh-CN.json'
import en from './en.json'
// elementUI
import elementZhCn from 'element-plus/es/locale/lang/zh-cn';
import elementEn from 'element-plus/es/locale/lang/en';

/**
 * 支持的语言
 */
export const SUPPORT_LOCALES = ['zh-CN', "en"] as const

export type SupportLocaleTuple = typeof SUPPORT_LOCALES

/**
 * [类型]支持的语言
 */
export type SupportLocale = SupportLocaleTuple[number]

// 中文语言包
const zhCNMessage = {
    element: elementZhCn,
    ...zhCN,
}

// 英文语言包
const enMessage = {
    element: elementEn,
    ...en,
}

// 词条 type
export type MessageSchema = typeof zhCNMessage

export const messages: Record<SupportLocale, MessageSchema> = {
    'zh-CN': zhCNMessage,
    'en': enMessage,
}