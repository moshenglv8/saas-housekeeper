/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 查询工人信息
 *
 * @author wwx1136431
 * @since 2022/3/18 15:18
 */
@Setter
@Getter
@ApiModel("查询工人信息")
public class GetWorkUserInfoDto {
    @ApiModelProperty(value = "工人id", required = false)
    private String userId;
}
