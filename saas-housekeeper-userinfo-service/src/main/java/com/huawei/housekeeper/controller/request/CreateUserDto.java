/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller.request;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.Length;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 创建用户信息对象
 *
 * @author l84165417
 * @since 2022-01-26
 */

@Setter
@Getter
@ApiModel("创建用户信息对象")
public class CreateUserDto {
    @ApiModelProperty(value = "用户名", required = true)
    @Length(min = 2, max = 10, message = "用户名长度范围: 2-10")
    private String userName;

    @ApiModelProperty(value = "密码", required = true)
    @Length(min = 6, max = 18, message = "长度范围: 6-18")
    private String password;

    @ApiModelProperty(value = "地址", required = false)
    private String address;

    @ApiModelProperty(value = "电话号码", required = false)
    private String phoneNo;

    @Email(message = "请输入正确的邮箱")
    @ApiModelProperty(value = "邮箱", required = false)
    private String email;

    @ApiModelProperty(value = "身份证号码", required = false)
    private String idCard;

    @Min(value = 1, message = "最小值:1")
    @Max(value = 2, message = "最大值:2")
    @ApiModelProperty(value = "1：用户注册,2: 工人注册", required = true)
    private Integer accountType;
}