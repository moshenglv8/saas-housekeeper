/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huawei.housekeeper.result.ListRes;
import com.huawei.housekeeper.controller.request.CreateUserDto;
import com.huawei.housekeeper.controller.request.GetUserDto;
import com.huawei.housekeeper.controller.request.GetWorkUserInfoDto;
import com.huawei.housekeeper.controller.request.PageQueryAllUserDto;
import com.huawei.housekeeper.controller.request.PageQueryUserDto;
import com.huawei.housekeeper.controller.response.GetTenantUserInfoVo;
import com.huawei.housekeeper.controller.response.GetUserInfoVo;
import com.huawei.housekeeper.controller.response.GetUserVo;
import com.huawei.housekeeper.controller.response.GetWorkUserInfoVo;
import com.huawei.housekeeper.dao.entity.User;

/**
 * 用户信息服务
 *
 * @author y00464350
 * @since 2022-02-14
 */
public interface UserService extends IService<User> {

    /**
     * 创建用户信息
     *
     * @param createUserInfoDto 用户dto
     * @return userId
     */
    String createUserInfo(CreateUserDto createUserInfoDto);

    /**
     * 分页查询用户信息
     *
     * @param pageQueryUserDto 分页请求
     * @return 用户列表
     */
    ListRes<GetUserVo> pageQueryUsers(PageQueryUserDto pageQueryUserDto);

    /**
     * 查询用户信息
     *
     * @param userDto 用户请求
     * @return UserVo
     */
    ListRes<GetUserVo> getUserInfo(GetUserDto userDto);

    /**
     * 删除用户信息
     *
     * @param userId 用户userId
     * @return boolean
     */
    Boolean deleteUserInfoByUserId(String userId);

    /**
     * 查询用户信息详情
     *
     * @param
     * @return UserVo
     */
    GetUserInfoVo getUserInfoById();

    /**
     * 租户查询用户信息列表
     *
     * @param pageQueryUserDto 用户请求
     * @return UserVo
     */
    ListRes<GetTenantUserInfoVo> getUserInfoList(PageQueryUserDto pageQueryUserDto);

    /**
     * 查询工人信息
     *
     * @param workUserInfoDto 工人请求
     * @return UserVo
     */
    GetWorkUserInfoVo getWorkInfo(GetWorkUserInfoDto workUserInfoDto);

    ListRes<GetTenantUserInfoVo> getAllUserInfoList(PageQueryAllUserDto pageQueryAllUserDto);
}
