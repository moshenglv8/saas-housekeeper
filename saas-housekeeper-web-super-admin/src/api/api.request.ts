import HttpRequest from './axios';

const baseUrl = '/api-admin/';
const axios = new HttpRequest(baseUrl);
export default axios;
