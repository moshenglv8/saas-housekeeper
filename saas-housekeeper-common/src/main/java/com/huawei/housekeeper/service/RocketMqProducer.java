package com.huawei.housekeeper.service;

import com.huawei.housekeeper.entity.BaseMqMessage;

public interface RocketMqProducer {
    public <T extends BaseMqMessage> void send(String destination, T message);
}
