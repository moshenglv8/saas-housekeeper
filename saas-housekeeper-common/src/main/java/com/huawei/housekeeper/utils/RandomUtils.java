package com.huawei.housekeeper.utils;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class RandomUtils {
    public static Integer code() throws NoSuchAlgorithmException {
        SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
        Integer mixNum = 100000;
        Integer maxNum = 999999;
        Integer randNum = random.nextInt(maxNum);
        if (randNum < mixNum) {
            randNum += mixNum;
        }
        return randNum;
    }
}