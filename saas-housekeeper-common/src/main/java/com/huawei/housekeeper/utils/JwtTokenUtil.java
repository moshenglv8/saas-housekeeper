/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.utils;

import com.huawei.housekeeper.constants.CommonConstants;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.Map;

/**
 * JwtToken生成的工具类
 * JWT token的格式：header.payload.signature
 * header的格式（算法、token的类型）：
 * {"alg": "HS512","typ": "JWT"}
 * payload的格式（用户名、创建时间、生成时间）：
 * {"sub":"wang","created":1489079981393,"exp":1489684781}
 * signature的生成算法：
 * HMACSHA512(base64UrlEncode(header) + "." +base64UrlEncode(payload),secret)
 *
 * @author y00464350
 * @since 2022-02-11
 */
@Component
public class JwtTokenUtil {

    private static final String CLAIM_KEY_CREATED = "created";

    @Value("${jwt.secret}")
    private String secret;

    @Value("${jwt.expiration}")
    private Long expiration;

    @Value("${jwt.tokenHead}")
    private String tokenHead;

    /**
     * 从token中获取jwt的负载
     *
     * @param token token
     * @return 负载
     */
    public Claims getClaimsFromToken(String token) {
        SecretKey secretKey = new SecretKeySpec(secret.getBytes(StandardCharsets.UTF_8), SignatureAlgorithm.HS512.getJcaName());
        Claims claims = Jwts.parserBuilder().setSigningKey(secretKey).build().parseClaimsJws(token).getBody();
        MDC.put("userId", (String) claims.get(Claims.SUBJECT));
        return claims;
    }

    /**
     * 从token中获取登录用户id
     *
     * @param token token值
     */
    public String getUserIdFromToken(String token) {
        Claims claims = getClaimsFromToken(token);
        return (String) claims.get(Claims.SUBJECT);
    }

    /**
     * 从token中获取用户密码
     *
     * @param token token值
     * @return 密码
     */
    public String getUserPasswordFromToken(String token) {
        Claims claims = getClaimsFromToken(token);
        return (String) claims.get(CommonConstants.User.USER_PAW);
    }

    /**
     * 从token中获取用户名
     *
     * @param token token值
     * @return 用户名
     */
    public String getUserNameFromToken(String token) {
        Claims claims = getClaimsFromToken(token);
        return (String) claims.get(CommonConstants.User.USER_NAME);
    }

    /**
     * 从token中获取domain
     *
     * @param token token值
     * @return 用户名
     */
    public String getSchemaFromToken(String token) {
        Claims claims = getClaimsFromToken(token);
        return (String) claims.get(CommonConstants.User.USER_DOMAIN);
    }

    /**
     * 判断token是否已经失效
     *
     * @param token token值
     * @return token是否失效
     */
    public boolean isTokenExpired(String token) {
        Date expiredDate = getExpiredDateFromToken(token);
        return expiredDate.before(new Date());
    }

    /**
     * 从token中获取过期时间
     *
     * @param token 客户端传入的token
     * @return boolean
     */
    public Date getExpiredDateFromToken(String token) {
        return getClaimsFromToken(token).getExpiration();
    }

    /**
     * 判断token是否可以被刷新
     *
     * @param token token值
     * @return boolean
     */
    public boolean canRefresh(String token) {
        return !isTokenExpired(token);
    }

    /**
     * 刷新 TOKEN
     *
     * @param token token值
     * @return 更新的token值
     */
    public String refreshToken(String token) {
        Claims claims = getClaimsFromToken(token);
        claims.put(CLAIM_KEY_CREATED, new Date());
        return generateToken(claims);
    }

    /**
     * 根据负载生成JWT的token
     */
    public String generateToken(Map<String, Object> claims) {
        SecretKey secretKey = new SecretKeySpec(secret.getBytes(StandardCharsets.UTF_8), SignatureAlgorithm.HS512.getJcaName());
        return tokenHead + Jwts.builder()
                .setClaims(claims)
                .setExpiration(generateExpirationDate())
                .signWith(secretKey)
                .compact();
    }

    /**
     * 生成token的过期时间
     */
    public Date generateExpirationDate() {
        return new Date(System.currentTimeMillis() + expiration * 1000);
    }
}